Overall plan for my system

* Software used in priority of what I want to work on:

- text editor :: emacs
  - Most extensible editor
- window manager :: stump
  - get a good work flow
- package management :: guix (nix)
  - i like having things in code
  - consider switching to using guix sd on one of my systems
- browser :: qutebrowser (nyxt)
  - Provides keyboard based web browser that works well and is configurable
- shell :: zsh
  - posix compliant (i think) yet very powerful
- terminal emulator :: st (alacritty,vterm)
  - alternatives: various emacs options, others
  - don't think i care much about what I do for this one honestly
- distribution :: endeavour (guix,nix)
  - i like the aur and pacman
- alternative web browser :: firefox (chromium)
  - for when websites don't like qute for some reason
- password manager :: bitwarden
  - good for convenience and i don't want to lose my passwords self hosting
  - downside is less accessible from code (unlike gpg for example)

* TODO clean up repo

- [ ] organize files more clearly, i.e. by program using them instead of by "desktop", "misc", etc
- [ ] rename scripts dir to bin
- [ ] clean up and update README files
