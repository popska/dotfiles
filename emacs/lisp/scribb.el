;;; -*- lexical-binding: t; -*-

;; 1.0.1 cutoff

(use-package ellama
  :if (not m-ww?)
  :defer 3
  :init
  ;; setup key bindings
  ;; (setopt ellama-keymap-prefix "C-c e")
  ;; language you want ellama to translate to
  (setopt ellama-language "English")
  ;; could be llm-openai for example
  (require 'llm-ollama)
  (setopt ellama-provider
          (make-llm-ollama
           ;; this model should be pulled to use it
           ;; value should be the same as you print in terminal during pull
           :chat-model "llama3"
           :embedding-model "nomic-embed-text"
           :default-chat-non-standard-params '(("num_ctx" . 8192))))

  ;; Naming new sessions with llm
  (setopt ellama-naming-scheme 'ellama-generate-name-by-llm))

(provide 'scribb)
