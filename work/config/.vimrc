set ic
set relativenumber
set number
set hlsearch

let mapleader = " "

map <leader>v :source $USERPROFILE\.vimrc

map <leader>( t(vi(
map <leader>) t)va)
map <leader>{ t{vi{
map <leader>} t}va}
map <leader>[ t[vi[
map <leader>] t]va]

" local leaders:
" r - refactoring
" g - navigating
" c - code language specific (breakpoints, compiling, etc.)

nnoremap <leader>cb :vsc Debug.ToggleBreakpoint<cr>
nnoremap <leader>cr :vsc TestExplorer.RunAllTestsInContext<cr>
nnoremap <leader>cR :vsc TestExplorer.DebugAllTestsInContext<cr>
nnoremap <leader>cc :vsc Build.BuildSelection<cr>

map <leader>rr :vsc Refactor.Rename<cr>
nnoremap <leader>rp :vsc Refactor.ReorderParameters<cr>

map <leader>gi :vsc Edit.GoToImplementation<cr>
map <leader>gr :vsc Edit.FindAllReferences<CR>
map <leader>gp :vsc Edit.PeekDefinition<CR>
map gc :vsc Edit.CommentSelection<cr>
map gC :vsc Edit.UncommentSelection<cr>
nnoremap <leader>gm :vsc Edit.NextMethod<cr>
nnoremap <leader>gM :vsc Edit.PreviousMethod<cr>
nnoremap <leader>ge :vsc View.NextError<cr>
nnoremap <leader>gE :vsc View.PreviousError<cr>
map <leader>pf :vsc Edit.GoToAll<cr>

" surround with for loop, if, etc.
vmap S :vsc Edit.SurroundWith<cr>
" vmap P :vsc Edit.Paste<cr>
xnoremap P "_dP

" just testing
nnoremap U :vsc Edit.Redo<cr>

" Use ZA and ZW for writing files so i never have to use :w or :wa
nmap ZA :wa<cr>
nmap ZW :w<cr>

" TODO: add a lot of the Debug.* to my key bindings
" TODO: checkout the Refactor.* bindings

" use system clipboard by default
set clipboard=unnamed
