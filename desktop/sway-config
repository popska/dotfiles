# -*- mode: conf -*-

# TODO: try a leader key like approach

set $mod Mod4

font pango:Fira Code 12

# swap caps and esc in sway
input * {
    xkb_layout "us"
    xkb_options "caps:escape"
}

# set wallpapers
output "HDMI-A-1" bg /home/popska/sfs/org/archive/pictures/20230625T140312--mountain-sunrise__wallpaper.jpg fill
output "eDP-1" bg /home/popska/sfs/org/archive/pictures/20230625T134854--girl-outline__wallpaper.jpg fill

# laptop:
output eDP-1 pos 0 0 res ${laptop-screen-width}x${laptop-screen-height}

# monitor:
output HDMI-A-1 pos ${laptop-screen-width} 0 res 1800x960
output HDMI-A-1 scale 1

# lock after i've been away
# TODO: bind key to swaylock
exec swayidle -w \
    timeout 600 '/home/popska/sfs/dotfiles/scripts/mlock' \
    timeout 610 '/home/popska/sfs/dotfiles/scripts/mlock' \
    timeout 60 'if pgrep -x swaylock; then swaymsg "output * power off"; fi' \
    resume 'swaymsg "output * power on"'

# border settings
hide_edge_borders smart
default_border pixel 4

# class                 border  backgr. text    indicator child_border
client.focused          #000000 #000000 #ffffff #2e9ef4   #ff00ff
#client.focused_inactive #333333 #5f676a #ffffff #484e50   #5f676a
#client.unfocused        #333333 #222222 #888888 #292d2e   #222222
#client.urgent           #2f343a #900000 #ffffff #900000   #900000
#client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c
#client.background       #ffffff

gaps inner 8
smart_gaps on

focus_follows_mouse no

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"


### basic navigation ###


# change focus
#bindsym $mod+h exec "/home/popska/sfs/dotfiles/scripts/windowfocus left"
#bindsym $mod+j exec "/home/popska/sfs/dotfiles/scripts/windowfocus down"
#bindsym $mod+k exec "/home/popska/sfs/dotfiles/scripts/windowfocus up"
#bindsym $mod+l exec "/home/popska/sfs/dotfiles/scripts/windowfocus right"
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# move workspace to other monitor
bindsym $mod+Control+h move workspace to output left
bindsym $mod+Control+l move workspace to output right

# switch to workspace
bindsym $mod+i workspace number $ws1
bindsym $mod+n workspace number $ws2
bindsym $mod+e workspace number $ws3
bindsym $mod+a workspace number $ws4
bindsym $mod+Comma workspace number $ws5

# move focused container to workspace
bindsym $mod+Shift+i move container to workspace number $ws1
bindsym $mod+Shift+n move container to workspace number $ws2
bindsym $mod+Shift+e move container to workspace number $ws3
bindsym $mod+Shift+a move container to workspace number $ws4
bindsym $mod+Shift+Comma move container to workspace number $ws5


### launch applications ###


# start a terminal
bindsym $mod+t exec "if pgrep emacs ; then emacsclient -c -e '(vterm)' ; else st -e fish ; fi"
bindsym $mod+Shift+t exec i3-sensible-terminal

# start common applications
bindsym $mod+r exec qutebrowser
bindsym $mod+s exec "emacsclient -c -n -a 'emacs'"

# start fuzzel (a program launcher)
bindsym $mod+f exec --no-startup-id fuzzel

# password manager
bindsym $mod+g exec passmenu --type -u
bindsym $mod+Shift+g exec passmenu --type

# change the brightness
mode "brightness (j, k, 0-9, [a]pex)" {
    bindsym j exec brightnessctl s 10%-
    bindsym k exec brightnessctl s +10%
    bindsym 0 exec brightnessctl s 0%
    bindsym 1 exec brightnessctl s 10%
    bindsym 2 exec brightnessctl s 20%
    bindsym 3 exec brightnessctl s 30%
    bindsym 4 exec brightnessctl s 40%
    bindsym 5 exec brightnessctl s 50%
    bindsym 6 exec brightnessctl s 60%
    bindsym 7 exec brightnessctl s 70%
    bindsym 8 exec brightnessctl s 80%
    bindsym 9 exec brightnessctl s 90%
    bindsym a exec brightnessctl s 100%


    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+b mode "default"
}
bindsym $mod+b mode "brightness (j, k, 0-9, [a]pex)"

# volume
set $refresh_i3status killall -SIGUSR1 i3status
mode "volume (j, k, 0-9, [a]pex, [m]ute)" {
    bindsym j exec amixer sset Master 10%-
    bindsym k exec amixer sset Master 10%+
    bindsym 0 exec amixer sset Master 00%
    bindsym 1 exec amixer sset Master 10%
    bindsym 2 exec amixer sset Master 20%
    bindsym 3 exec amixer sset Master 30%
    bindsym 4 exec amixer sset Master 40%
    bindsym 5 exec amixer sset Master 50%
    bindsym 6 exec amixer sset Master 60%
    bindsym 7 exec amixer sset Master 70%
    bindsym 8 exec amixer sset Master 80%
    bindsym 9 exec amixer sset Master 90%
    bindsym a exec amixer sset Master 100%
    bindsym m exec amixer sset Master toggle


    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+v mode "default"
}
bindsym $mod+v mode "volume (j, k, 0-9, [a]pex, [m]ute)"
#bindsym $mod+i exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
#bindsym $mod+u exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
#bindsym $mod+m exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
#bindsym $mod+Shift+m exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status


### restarting/reloading/killing ###


# kill focused window
bindsym $mod+c kill
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+c exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# reload the configuration file
bindsym $mod+w reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+w restart


### splits and layout


# split in horizontal orientation
bindsym $mod+y split h

# split in vertical orientation
bindsym $mod+p split v

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+o layout tabbed
bindsym $mod+u layout toggle split

## fullscreen/floating stuff

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# enter fullscreen mode for the focused container
bindsym $mod+Escape fullscreen toggle

# toggle tiling / floating
bindsym $mod+Return floating toggle
bindsym $mod+slash move position center

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    bindsym h resize shrink width 10 px or 10 ppt
    bindsym j resize grow height 10 px or 10 ppt
    bindsym k resize shrink height 10 px or 10 ppt
    bindsym l resize grow width 10 px or 10 ppt

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape or $mod+r
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+Period mode "default"
}

bindsym $mod+Period mode "resize"


### bar & rest ###


# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
    # i3bar_command i3bar --transparency
    status_command /home/popska/sfs/dotfiles/desktop/statusbar
    mode hide
    position top
}

# change focus between tiling / floating windows
#bindsym $mod+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# startup
exec dunst
exec /home/popska/sfs/proj/safe/run-like-cron

# TODO: bind these
# grim -g "$(swaymsg -t get_tree | jq -j '.. | select(.type?) | select(.focused).rect | "\(.x),\(.y) \(.width)x\(.height)"')" img/q2-sliced.png # change the output
# grim -g "$(slurp)"
