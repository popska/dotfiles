;;; -*- buffer-read-only: t; -*-
(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "5481a96417fbf94fc8b6a73abe72a97ce6bc6ec1")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))
      (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        (branch "master")
        (commit
          "faf4c7dcf3f772b369bbf4b964aa4f5e0631dd69")
        (introduction
          (make-channel-introduction
            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
            (openpgp-fingerprint
              "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
      (channel
        (name 'oniichan)
        (url "https://gitlab.com/popska/oniichan")
        (branch "master")
        (commit
          "5fab61287af73fb643c2d55dedca599c36a233cc")
        (introduction
          (make-channel-introduction
            "541945c3aa368de7725166df08ccdacd9195bc19"
            (openpgp-fingerprint
              "CC64 7379 9190 2041 F708  6F2C 3C67 9FA8 EC6C E1C7")))))

