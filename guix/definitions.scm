(use-modules
 (gnu home services)
 (gnu home services shells)
 (gnu home services gnupg)
 (gnu home services guix)
 (gnu home services mcron)
 (gnu home services pm) ; batsignal service
 (gnu packages)
 (gnu packages texinfo) ; provides texinfo
 (gnu packages gnupg) ; provides pinentry packages for service
 (gnu packages linux) ; provides inotify-tools
 (gnu services) ; provides (simple-service)
 (guix gexp) ; provides (local-file)
 (guix channels) ; provides (channel)
 (srfi srfi-1) ; provides (fold)
 (guix describe) ; provides (current-channels)
 (ice-9 pretty-print)
 (guix modules) ; provides (source-module-closure)
 (guix packages) ; provides (package)
 (guix git-download) ; provides (git-fetch)
 (guix build-system emacs) ; provides emacs-build-system
 (guix build-system copy) ; provides copy-build-system
 (gnu home services shepherd) ; home-shepherd-service-type
 (gnu packages emacs) ; provides emacs for daemon service
 (gnu home services syncthing)
 ((guix licenses) #:prefix license:))


;;; custom package definitions
(define st
  (package
   (inherit (specification->package "st"))
   (name "st")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/popska/st")
                  (commit "6dc8c205305d0c9a5e7543c00e138bde0e6b2ec1")))
            (sha256
             (base32 "000jx82q0m0flw91ksldkqsp5fq62y1k5gwx4d1spmpgvmavm33f"))))))

(define-public zsh-vi-mode
  (package
   (name "zsh-vi-mode")
   (version "0.11.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/jeffreytse/zsh-vi-mode")
                  (commit (string-append "v" version))))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0bs5p6p5846hcgf3rb234yzq87rfjs18gfha9w0y0nf5jif23dy5"))))
   (build-system copy-build-system)
   (arguments
    '(#:install-plan '(("zsh-vi-mode.plugin.zsh" "share/zsh/plugins/zsh-vi-mode/")
                       ("zsh-vi-mode.zsh" "share/zsh/plugins/zsh-vi-mode/")
                       ("README.md" "share/doc/zsh-vi-mode/"))))
   (home-page "https://github.com/jeffreytse/zsh-vi-mode")
   (synopsis "...")
   (description "...")
   (license license:expat)))


;;; packages
;; shared between all systems
(define common-packages
  (->packages
   "neofetch"
   "htop"
   "ripgrep" ; used primarily by emacs (consult)
   "tree"
   "unison" ; file syncing
   "zip"
   "unzip"
   "which"
   "jq" ; used by dotfiles/desktop/statusbar (and probably more)
   "cloc"
   "ascii"
   "inotify-tools" ; file watcher
   "tmux"

   "plantuml"

   ;; zsh stuff
   zsh-vi-mode
   "zsh-syntax-highlighting"
   "zsh-autopair"
   "zsh-autosuggestions"
   ))

;; for equanimity and sublimity
(define personal-packages
  (->packages
   ;; HACK: have to install fakeroot here for yay to work right.
   "fakeroot"

   "mandoc" ; man command
   "coreutils-minimal"
   "less"

   "git"
   "openssh"

   "curl"
   "nss-certs"

   "ungoogled-chromium"
   "qutebrowser"

   st ; my fork of st

   "duplicity" ; encrypted file backup
   "font-fira-code"
   "font-fira-sans"
   "mpv"

   "imagemagick"
   "ghostscript"

   ;; TODO: configure dunst
   "dunst" ; notifications
   "brightnessctl"
   "fuzzel" ; alternative to dmenu
   "grim" ; screenshots in wayland
   "slurp" ; select region in wayland (useful for grim)
   "wl-clipboard" ; interact with clipboard in wayland
   "sway"
   "i3status"
   "swaybg"
   "swayidle"

   "clojure"
   "clojure-tools" ; provides repl, etc
   "openjdk" ; i assume latest version is ok

   "leiningen"
   "openscad"

   ;;"libreoffice" ;; TMP build broken?

   "gnupg"
   "pinentry"
   ;; "pinentry-tty"
   ;; "pinentry-gtk2"

   ;; uncomment if you want guix to take 10x as long...
   ;;"texlive"

   ;;"emacs-pgtk" ; use for sway
   "emacs" ; the beast...

   "scrot" ; screenshots in x

   ;; stump stuff
   "stumpwm-with-slynk" ; this version has repl built in
   "stumpish" ; command line access
   "sbcl" ; compiler

   ;; For some reason I have to pull in a shit ton of dependencies of the ttf fonts:
   "sbcl-stumpwm-ttf-fonts" ; fix fonts
   "sbcl-clx-truetype"
   "sbcl-zpb-ttf"
   "sbcl-cl-vectors"
   "sbcl-cl-fad"
   "sbcl-bordeaux-threads"
   "sbcl-global-vars"
   "sbcl-trivial-features"
   "sbcl-trivial-garbage"
   "sbcl-cl-store"

   ;; other modules
   "sbcl-stumpwm-battery-portable"

   ;; get newer version of the stump pamixer package which supports mic in modeline
   (package
    (inherit (specification->package "sbcl-stumpwm-pamixer"))
    (name "sbcl-stumpwm-pamixer")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/Junker/stumpwm-pamixer.git")
                   (commit "5ea5e7af9ca6410f23f7de24b77a3e984769e09d")))
             (sha256
              (base32 "17ykc3hl8i1i2dsd20ii7fn5dxsph7mj85fzv0sa6nfrf4lzg8k3")))))

   ;; xorg
   "xrandr"
   "arandr"
   "xsetroot" ; allows changing cursor
   "xmodmap" ; keyboard layouts
   "xsel" ;clipboard

   ;; guix development stuff
   "guile"
   "guile-gnutls"
   "gnutls"
   ))

;; just for work
(define work-packages
  (->packages
   "emacs-no-x" ; don't use gui because it's slower
   ))

;; just for equanimity
(define equanimity-packages
  (->packages
   ;; "signal-desktop"
   ;; "steam"
   "zoom"
   "vips" ; fast and lightweight large image processing (research)

   ;; stats class
   "r"
   "r-ggplot2"
   "r-knitr"
   ))

;; just for sublimity
(define sublimity-packages
  (->packages
   "zoom"
   "vips" ; fast and lightweight large image processing (research)

   ;; installing the following with yay
   ;; "go" ; for adv soft eng
   ;; "gopls" ; lsp server for go
   ;; "docker" ; for adv soft eng
   ))


;;; config files

;; common to all machines
(define* (common-homefiles #:optional (template-alist '()))
  (append
   (stow "dotfiles/config/.global-gitignore"
         ".config/git/ignore")
   (stow "dotfiles/emacs/init.el"
         ".config/emacs/init.el"
         #:develop?! #t)
   (stow "dotfiles/emacs/lisp"
         ".config/emacs/lisp"
         #:develop?! #t
         #:recursive? #t)
   (stow "dotfiles/config/tmux.conf"
         ".config/tmux/tmux.conf"
         #:develop?! #t)))

;; specific to sublimity and equanimity
(define* (personal-homefiles #:optional (template-alist '()))
  (append
   ;; xdg
   (stow "dotfiles/desktop/swaybar.conf"
         ".config/i3status/config")
   (stow "dotfiles/config/qutebrowser.py"
         ".config/qutebrowser/config.py")
   (stow "dotfiles/config/.my-gitconfig"
         ".config/git/config")
   (stow "dotfiles/desktop/sway-config"
         ".config/sway/config"
          #:template-alist template-alist)
   (stow "dotfiles/desktop/stumpwm.lisp"
         ".config/stumpwm/config"
         #:develop?! #t)

   ;; home
   (stow "dotfiles/kbd/aurora"
         "sfs/proj/qmk_firmware/keyboards/splitkb/aurora/sweep/keymaps/popska"
         #:recursive? #t)
   (stow "dotfiles/kbd/ergodox"
         "sfs/proj/qmk_firmware/keyboards/ergodox_ez/keymaps/popska"
         #:develop?! #t
         #:recursive? #t)
   (stow "dotfiles/misc/.mbsyncrc"
         ".mbsyncrc")
   (stow "secrets/.authinfo.gpg"
         ".authinfo.gpg")
   (stow "dotfiles/desktop/.xinitrc"
         ".xinitrc"
         #:develop?! #t)
   (stow "dotfiles/kbd/.Xmodmap"
         ".Xmodmap"
         #:develop?! #t)
   ;; TODO: get the name of the folder smartly, probably by searching .mozilla for the prefs.js file?
   (stow "dotfiles/config/moz-user.js"
         ".mozilla/firefox/ezw1ahee.default-release/user.js")))


;;; services
(define* (make-home-files-service #:optional (homefiles '()))
  (simple-service
   'home-files-service
   home-files-service-type
   homefiles))

(define (make-bash-service)
  (service home-bash-service-type
           (home-bash-configuration
            (aliases '(("grep" . "grep --color=auto")
                       ("ll" . "ls -l")
                       ("ls" . "ls -p --color=auto")))
            (bashrc (list (local-file
                           "../config/.bashrc"
                           "bashrc")))
            (bash-profile (list (local-file
                                 "../config/.bash_profile"
                                 "bash_profile"))))))

(define (make-zsh-service)
  (service home-zsh-service-type
           (home-zsh-configuration
            (zshrc (list (local-file
                          "../config/.zshrc"
                          "zshrc")
                         (plain-file
                          "zshrc"
                          (apply string-append
                                 (append
                                  ;; autopair conflicts with vi-mode. fix this by deferring autopair init
                                  ;; and then doing it at the very end
                                  ;; see https://github.com/hlissner/zsh-autopair?tab=readme-ov-file#troubleshooting--compatibility-issues
                                  ;; and https://github.com/jeffreytse/zsh-vi-mode?tab=readme-ov-file#execute-extra-commands
                                  '("AUTOPAIR_INHIBIT_INIT=1\n")
                                  (map (lambda (plugin)
                                         (format #f "source ~a/share/zsh/plugins/~a/~a.zsh\n"
                                                 (get-guix-store-path plugin)
                                                 (struct-ref plugin 0)
                                                 (struct-ref plugin 0)))
                                       (list zsh-vi-mode
                                             (specification->package "zsh-autosuggestions")
                                             (specification->package "zsh-autopair")
                                             (specification->package "zsh-syntax-highlighting")))
                                  '("zvm_after_init_commands+=('autopair-init')\n")))))))))

;; tell guix which channels to use
(define (make-channels-service)
  (simple-service
   'my-channels-service
   home-channels-service-type
   (list
    (channel
     (name 'nonguix)
     (url "https://gitlab.com/nonguix/nonguix")
     (branch "master")
     (introduction
      (make-channel-introduction
       "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
       (openpgp-fingerprint
        "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
    (channel
     (name 'oniichan)
     (url "https://gitlab.com/popska/oniichan")
     (branch "master")
     (introduction
      (make-channel-introduction
       "541945c3aa368de7725166df08ccdacd9195bc19"
       (openpgp-fingerprint
        "CC64 7379 9190 2041 F708  6F2C 3C67 9FA8 EC6C E1C7")))))))

;; mcron
(define (make-cron-service)
  (simple-service
   'cron-service
   home-mcron-service-type
   (list
    ;; garabage collect things not used for 2 months if less than 20GB are free
    #~(job "0 3 1 * *" ; 3am on the first
           "guix gc -d 2m -F 20G")
    ;; delete trash folder automatically
    #~(job "0 3 * * 0" ; sunday 3am
           "rm -rf $HOME/.local/share/Trash/*"))))

;; send notifications when battery is low on laptop
(define (make-batsignal-service)
  (simple-service
   'batsignal-service
   home-batsignal-service-type
   (home-batsignal-configuration
    (warning-level 20)
    (critical-level 10)
    (danger-level 5)
    (danger-command "notify-send \"battery dangerously low... saving emacs buffers\" ; emacsclient -e '(save-some-buffers t)'")
    (notifications? #t))))

;; configure gpg, pinentry, ssh
(define (make-gpg-service)
  (service
   home-gpg-agent-service-type
   (home-gpg-agent-configuration
    ;; XXX pinentry broke for some reason
    ;; (pinentry-program
    ;;  (file-append pinentry "/bin/pinentry-gtk-2"))
    (pinentry-program
     (file-append pinentry "/bin/pinentry"))
    (default-cache-ttl (* 8 60 60))
    (max-cache-ttl (* 24 60 60))
    (ssh-support? #t)
    (default-cache-ttl-ssh (* 8 60 60))
    (max-cache-ttl-ssh (* 24 60 60))
    ;; not sure why this isnt in there from ssh-support? #t
    (extra-content "enable-ssh-support
allow-emacs-pinentry
allow-loopback-pinentry
"))))

;; set environment variable. these are sourced by login shell
(define* (make-env-vars-service #:optional (env-vars '()))
  (simple-service
   'env-vars-service
   home-environment-variables-service-type
   env-vars))

(define common-env-vars
  `(
    ;; applications installed via guix don't use system locale, so we
    ;; have to manage that ourselves
    ;; see https://guix.gnu.org/manual/en/html_node/Application-Setup.html#Locales
    ;; i had to run `guix install glibc-locales` manually,
    ;; specifying it in this file didn't work for some reason
    ("GUIX_LOCPATH" . "$HOME/.guix-profile/lib/locale")
    ("_GUIX_HOME_MODE" . "develop")
    ("EDITOR" . "ed")
    ("PATH" . ,(let* ((paths `(,(prepend-dotfiles-repo "scripts")
                               "$PATH"))
                      (reversed-paths (reverse paths)))
                 (fold (lambda (str0 str1) (string-append str0 ":" str1))
                       (car reversed-paths)
                       (cdr reversed-paths))))
    ;; HACK: modus thems info page is in same path as emacs, so emacs' version gets loaded
    ;; The emacs version is wrong version (I use version 4.2)
    ("INFOPATH" . ,(string-append (get-guix-store-path emacs-modus-themes)
                                  "/share/info"
                                  ":$INFOPATH"))
    ("SFS_DIR" . ,(prepend-sfs-dir ""))
    ("DOTFILES_DIR" . ,(prepend-dotfiles-repo ""))))

(define personal-env-vars
  `(#;("VAR" . "VALUE...")
    ;; make R be able to find guix installed libraries
    ("R_LIBS" . "$HOME/.guix-home/profile/site-library")
    ;; HACK this should be removed after next guix pull. see https://issues.guix.gnu.org/75709
    ("EMACSNATIVELOADPATH" . "$HOME/.guix-home/profile/lib/emacs/native-site-lisp")))

(define* (make-home-activation-service #:rest gexps)
  (simple-service
   'activation-service
   home-activation-service-type
   #~(begin #$@gexps)))

;; maybe useful https://www.reddit.com/r/GUIX/comments/1cpmgwl/simpleservice_what_am_i_missing/
;; TODO: get this working correctly
;; inotifywait -r -e close_write,moved_to,create -m content ./*.el Makefile |
(define (make-home-template-service)
  (simple-service
   'home-template
   home-shepherd-service-type
   (list
    (shepherd-service
     (requirement '()) ; is this ok?
     (provision '(home-template))
     (stop  #~(make-kill-destructor))
     (start #~(make-forkexec-constructor
               (list #$(file-append inotify-tools "/bin/inotifywait")
                     "-r" "-e" "close_write,moved_to,create" "-m" (string-append (getenv "HOME") "/sfs/dotfiles"))
               #:log-file (string-append
                           (or (getenv "XDG_LOG_HOME")
                               (format #f "~a/.local/var/log"
                                       (getenv "HOME")))
                           "/template-service.log")))))))

;; have emacs run in background, so I can easily connect to it.
(define (make-emacs-daemon-service)
  (simple-service
   'emacs-daemon
   home-shepherd-service-type
   (list
    (shepherd-service
     (requirement '()) ; idk what this for...
     (provision '(emacs-daemon))
     (stop #~(make-kill-destructor))
     (start #~(make-forkexec-constructor
               (list #$(file-append emacs "/bin/emacs") "--fg-daemon")
               #:log-file (string-append
                           (or (getenv "XDG_LOG_HOME")
                               (format #f "~a/.local/var/log"
                                       (getenv "HOME")))
                           "/emacs-daemon.log")))))))

(define (make-syncthing-service)
  (service home-syncthing-service-type
           (for-home
            (syncthing-configuration (user "popska")
                                     (home (string-append (getenv "HOME") "/sfs/sync"))))))

(define (get-current-channels-pretty)
  (call-with-output-string
   (lambda (port)
     (pretty-print `(list ,@(sort (map channel->code (current-channels))
                                  (lambda (channel0 channel1)
                                    (string< (format #f "~a" channel0) (format #f "~a" channel1)))))
                   port))))

(define write-channels-to-file-gexp
  #~(if (stat #$(prepend-dotfiles-repo "guix/channels.scm") #f)
        (let ((output-port (open-file #$(prepend-dotfiles-repo "guix/channels.scm") "w")))
          (display #$(string-append
                      ";;; -*- buffer-read-only: t; -*-\n"
                      (get-current-channels-pretty))
                   output-port)
          (newline output-port)
          (close output-port))))


;;; template values
(define sublimity-alist
  `((laptop-screen-width . ,(lambda () 1920))
    (laptop-screen-height . ,(lambda () 1200))))

(define equanimity-alist
  `((laptop-screen-width . ,(lambda () 1600))
    (laptop-screen-height . ,(lambda () 900))))

(define common-alist
  `())
