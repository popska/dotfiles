(use-modules
 (ice-9 ftw) ; (ftw)
 (ice-9 regex) ; (string-match) (regexp-substitute/global)
 (guix derivations) ; (derivation->output-path)
 (guix store) ; (use-with-store)
 (guix gexp) ; (lower-object) (plain-file)
 (ice-9 textual-ports) ; (get-string-all)
 (gnu packages) ; specification->package
 )


;;; define whether to mutate system
(define guix-home-mode
  (if (equal? (getenv "_GUIX_HOME_MODE") "develop")
      'develop
      'deploy))

(if (eq? guix-home-mode 'develop)
    (format #t "Allowing mutation of home system (i.e. symlinks to non gnu store files)\n")
    (format #t "Not allowing mutation of home system and preferring guix way for everything\n"))


;;; directory variables
(define sfs-directory-base (string-append (getenv "HOME") "/sfs/"))

(define dotfiles-repo-base (string-append sfs-directory-base "dotfiles/"))

(define (make-prepend-fn prefix) ; helper
  (lambda (file)
    (if (string-match prefix file) file (string-append prefix file))))

(define prepend-sfs-dir (make-prepend-fn sfs-directory-base))

(define prepend-dotfiles-repo (make-prepend-fn dotfiles-repo-base))

(define prepend-xdg-dir
  (make-prepend-fn (string-append
                    (or (getenv "XDG_CONFIG_HOME") "/home/popska/.config") "/")))

(define prepend-home-dir
  (make-prepend-fn (string-append
                    (or (getenv "HOME") "/home/popska") "/")))


;;; file system utilities
(define directory-files-recursively
  (lambda* (dir #:optional (pred (lambda (_filename _statinfo _flag) #t)))
    "recursively get files from DIR"
    (define output '())
    (ftw dir
         (lambda (filename statinfo flag)
           (begin
             (when (and (equal? flag 'regular) (pred filename statinfo flag))
               (set! output (cons filename output)))
             #t)))
    output))

(define (file-basename file)
  "get file name with no preceding directories. If FILE is a directory, get the childmost directory"
  (let ((split (string-split file #\/)))
    (if (= 1 (length split))
        (car split)
        (let ((last-two (list-tail split (- (length split) 2))))
          (if (not (string= (cadr last-two) ""))
              (cadr last-two)
              (car last-two))))))

(define (mkdir-p! dir)
  (let loop ((split (string-split dir #\/))
             (i 1))
    (if (<= i (length split))
        (let ((partial-dir (string-join (list-head split i) "/")))
          (if (and (> (string-length partial-dir) 0)
                   (not (stat partial-dir #f)))
              (mkdir partial-dir))
          (loop split (+ 1 i))))))

;; This doesn't provide the backup safety that the guix home service provides, nor does it clean up after itself!
(define (symlink-file! link real)
  "Symlink link to point to real"
  (let ((dir (dirname link)))
    (if (not (stat dir #f))
        (mkdir-p! dir))
    ;; delete the file or link if it already exists
    (if (false-if-exception (lstat link))
        (begin
          (format #t "Deleting file/dir/link ~a to make room to symlink it to ~a\n" link real)
          (delete-file link))
        (format #t "symlinking ~a to ~a\n" link real))
    (symlink real link)))


;;; utilities for putting local files into gnu store
(define (generate-guix-name path)
  "Makes a name that guix services are OK with"
  (apply string-append (map match:substring (list-matches "[a-zA-Z]+" path))))

(define (make-file-recursively-fn prepend-dir-fn) ; helper
  (lambda* (func link-dir real-dir #:optional pred)
    (map (lambda (file)
           (func
            (string-append link-dir
                           (string-drop
                            file
                            (cdr (vector-ref (string-match (file-basename real-dir) file) 1))))
            file
            (generate-guix-name file)))
         (apply directory-files-recursively
                (append (list (prepend-dir-fn real-dir)) (if pred (list pred) '()))))))

(define make-develop-fn
  (lambda (link-dir-fn real-dir-fn)
    (lambda* (link file _filename)
      (symlink-file! (link-dir-fn link) (real-dir-fn file))
      '())))

(define make-local-file-fn
  (lambda (link-dir-fn real-dir-fn)
    (lambda (link file filename)
      (let ((full-link-path (link-dir-fn link)))
        (when (and (stat full-link-path #f)
                   (equal? 'symlink (stat:type (lstat full-link-path)))
                   (string-contains (readlink full-link-path) sfs-directory-base))
          (format #t "deleting symlink at ~a to ~a\n" full-link-path (readlink full-link-path))
          (delete-file full-link-path)))
      (list link (local-file (real-dir-fn file) filename)))))

(define xdg->develop!
  #;"Takes 3 arguments:
  - path relative to $XDG_CONFIG_DIR to put the config file
  - path relative to dotfiles repo to get the file from
  - what to name the file (unused)
  Symlinks the XDG file path to point to the dotfile path and
  returns empty list"
  (if (eq? guix-home-mode 'develop)
      (make-develop-fn prepend-xdg-dir prepend-dotfiles-repo)
      (make-local-file-fn prepend-xdg-dir prepend-dotfiles-repo)))
(define xdg->local-file
  #;"Takes 3 arguments:
  - path relative to $XDG_CONFIG_DIR to put the config file
  - path relative to dotfiles repo to get the file from
  - what to name the file (used by guix)
  Symlinks puts the local file in gnu store and returns the object
  to pass to xdg-files-service which tells guix to symlink them."
  (make-local-file-fn prepend-xdg-dir prepend-dotfiles-repo))
(define xdg-files-recursively
  #;"Used to recursively stow a directory of files in gnu store.
  Takes 4 arguments:
  - Function to use to store the files (xdg->develop! or xdg->local-file)
  - Directory in $XDG_CONFIG_DIR to put all the files in
  - Directory relative to dotfiles repo
  - (optional) predicate function to determine if a file should be included
  Returns list of which is what the xdg->* function returned."
  (make-file-recursively-fn prepend-dotfiles-repo))

(define home->develop!
  #;"Takes 3 arguments:
  - path relative to $HOME to put the config file
  - path relative to sfs dir to get the file from
  - what to name the file (unused)
  Symlinks the HOME file path to point to the dotfile path and
  returns empty list"
  (if (eq? guix-home-mode 'develop)
      (make-develop-fn prepend-home-dir prepend-sfs-dir)
      (make-local-file-fn prepend-home-dir prepend-sfs-dir)))
(define home->local-file
  #;"Takes 3 arguments:
  - path relative to $HOME to put the config file
  - path relative to sfs dir to get the file from
  - what to name the file (used by guix)
  Symlinks puts the local file in gnu store and returns the object
  to pass to home-files-service which tells guix to symlink them."
  (make-local-file-fn prepend-home-dir prepend-sfs-dir))
(define home-files-recursively
  #;"Used to recursively stow a directory of files in gnu store.
  Takes 4 arguments:
  - Function to use to store the files (home->develop! or home->local-file)
  - Directory in $HOME to put all the files in
  - Directory relative to dotfiles repo
  - (optional) predicate function to determine if a file should be included
  Returns list of which is what the home->* function returned."
  (make-file-recursively-fn prepend-sfs-dir))

;; Thanks David!
;; https://github.com/daviwil/dotfiles/blob/be611d01ab183bfe5d6242916c053c397132be70/daviwil/utils.scm#L35
(define (apply-template template-string value-alist)
  (regexp-substitute/global
   #f
   "\\$\\{([A-Za-z/\\-]+)\\}"
   template-string
   'pre
   (lambda (m)
     (let* ((key (string->symbol (match:substring m 1)))
            (entry (assq key value-alist)))
       (if entry
           ((cdr entry))
           (raise-exception (format #f "Apply template key not found: ~a" key)))))
   'post))

(define (apply-template-file file-path value-alist)
  (call-with-input-file file-path
    (lambda (port)
      (apply-template (get-string-all port)
                      value-alist))))

(define (templated-file name filepath alist)
  #;"Used to replace strings in a templated file and return a guix
  file-like-object via (plain-file)"
  (plain-file name (apply-template-file filepath alist)))

(define* (stow source destination
               #:key (develop?! #f) (template-alist #f) (recursive? #f) (predicate #f))
  "Called like so:
'dotfiles/config/qutebrowser.py' ; [req] relative to sfs dir
'.config/qutebrowser/config.py' ; [req] relative to ~ (XDG_CONFIG_HOME assumed .config for now)
#:develop #t ; defaults to #f
#:template template-alist ; defaults to '(), meaning template not applied
#:recursive #f ; default #f
#:predicate #f ; optional predicate function (only useful if recursive)

Returns a list with 0 or more values to be passed to the home files service.
"
  (when (and template-alist develop?!)
    (error "WARNING template currently not expected to work with develop or recursive\n"))
  ;; 4 cases for individual file:
  ;; - local-file
  ;; - develop!
  ;; - templated-file
  ;; - TODO templated-develop!
  (let ((file-func (cond
                    ;; ((and develop?! template-alist) ...)
                    (template-alist (lambda (destination source name)
                                      (list destination
                                            (templated-file name
                                                            (prepend-sfs-dir source)
                                                            template-alist))))
                    (develop?! home->develop!)
                    (#t home->local-file))))
    (filter
     (lambda (file) (not (null? file)))
     (if recursive?
         (home-files-recursively
          file-func
          destination
          source
          predicate)
         (list
          (file-func
           destination
           source
           (generate-guix-name source)))))))

;;; useful if a configuration file needs a path in gnu store
(define (get-guix-store-path package)
  (derivation->output-path (with-store store (run-with-store store (lower-object package)))))

;;; package functions
(define (->packages . packages)
  (map (lambda (package)
         (if (string? package)
             (specification->package package)
             package))
       packages))
